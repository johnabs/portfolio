---
# Display name
title: John Biechele-Speziale 

# Is this the primary user of the site?
superuser: true

# Role/position/tagline
role: Graduate Research Assistant in Complex Systems 

# Organizations/Affiliations to show in About widget
organizations:
  - name: Purdue University
    url: https://www.purdue.edu/

# Short bio (displayed in user profile at end of posts)
bio: TBD 

# Interests to show in About widget
interests:
  - Complex Systems 
  - Emergence 
  - Algorithmic Information Dynamics 
  - Optimization 

# Education to show in About widget
education:
  courses:
    - course: PhD in Industrial Engineering 
      institution: Purdue University
      year: In Progress
    - course: MSc in Chemistry 
      institution: Purdue University 
      year: 2019
    - course: BSc in Chemistry 
      institution: Western Kentucky University 
      year: 2015

# Social/Academic Networking
# For available icons, see: https://wowchemy.com/docs/getting-started/page-builder/#icons
#   For an email link, use "fas" icon pack, "envelope" icon, and a link in the
#   form "mailto:your-email@example.com" or "/#contact" for contact widget.
social:
  - icon: envelope
    icon_pack: fas
    link: 'mailto:jbiechel@purdue.edu'
  - icon: mastodon 
    icon_pack: fab
    link: https://qoto.org/@johnabs
  - icon: graduation-cap # Alternatively, use `google-scholar` icon from `ai` icon pack
    icon_pack: fas
    link: https://scholar.google.com/citations?user=UhTvv7gAAAAJ&hl=en 
  - icon: gitlab
    icon_pack: fab
    link: https://gitlab.com/johnabs
  - icon: orcid
    icon_pack: ai
    link: https://orcid.org/0000-0003-0447-3350 
 # - icon: linkedin
 #   icon_pack: fab
 #   link: https://www.linkedin.com/

# Link to a PDF of your resume/CV.
# To use: copy your resume to `static/uploads/resume.pdf`, enable `ai` icons in `params.toml`,
# and uncomment the lines below.
# - icon: cv
#   icon_pack: ai
#   link: uploads/resume.pdf

# Enter email to display Gravatar (if Gravatar enabled in Config)
email: ''

# Highlight the author in author lists? (true/false)
highlight_name: true
---

I'm a Graduate Research Assistant in the Mario Ventresca group at Purdue University. My academic interests cover a wide area, but are succinctly summarized as interest in complex systems, optimization, automated design, and system dynamics. I'm also a co-founder/CTO in an environmental startup called Evergreen.

My hobbies include cooking/baking, keeping up with meme culture, woodworking, playing with my dog, spending too much time programming, and playing the occasional FromSoft game when I can squeak it in.

{{< icon name="download" pack="fas" >}} Download my {{< staticref "uploads/resume.pdf" "newtab" >}}resumé{{< /staticref >}}, or check out the Experience and Awards sections below.
