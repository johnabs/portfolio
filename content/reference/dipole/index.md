---
title: '1-nm-Wide Hydrated Dipole Arrays Regulate AuNW Assembly on Striped Monolayers in Nonpolar Solvent'

# Authors
# If you created a profile for a user (e.g. the default `admin` user), write the username (folder name) here
# and it will be replaced with their full name and linked to their profile.
authors:
  - Ashlin G. Porter 
  - Tianhong Ouyang 
  - Tyler R. Hayes 
  - admin 
  - Shane R. Russell 
  - Shelley A. Claridge 

date: '2019-08-08T00:00:00Z'
doi: ''

# Schedule page publish date (NOT publication's date).
publishDate: '2022-04-18T00:00:00Z'

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ['2']

# Publication name and optional abbreviated publication name.
publication: Chem 
publication_short: In *Chem*

abstract: We show that striped phases of horizontally oriented phospholipids presenting 1-nm-wide orientable dipole arrays can order and straighten flexible 2-nm-diameter gold nanowires (AuNWs) with lengths (up to 1 μm) that greatly exceed the template pitch (∼7.5 nm). AuNW ordering can extend over areas > 100 μm2. Whereas wires bundle in solution, with contact between ligand shells, wires adsorbing to the template separate to much larger center-to-center distances (e.g., 12–30 nm) near multiples of the template pitch, consistent with repulsive interactions between wires emerging during interactions with dipole arrays in the template. AuNW assembly was carried out in cyclohexane, a nonpolar solvent. Interestingly, we found that wire ordering was contingent on the extent of phospholipid headgroup hydration and the presence of excess oleylamine, which assembled into hemicylindrical micelles around the hydrated headgroups. Together, these components generated a protected polar environment that enabled the phospholipid headgroups to function collectively to regulate AuNW assembly. 

# Summary. An optional shortened abstract.
summary: Fat on graphene makes gold nanowires form in a patterned way. 

tags: []

# Display this page in the Featured widget?
featured: true

# Custom links (uncomment lines below)
# links:
# - name: Custom Link
#   url: http://example.org

url_pdf: 'https://doi.org/10.1016/j.chempr.2019.07.002'
url_code: ''
url_dataset: ''
url_poster: ''
url_project: ''
url_slides: ''
url_source: ''
url_video: ''

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
image:
  caption: 'Image credit: [**Chem**](https://doi.org/10.1016/j.chempr.2019.07.002)' 
  focal_point: ''
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
slides: ""
---

