---
title: 'A facile preparation of highly fluorescent carbon nitride nanoparticles via solid state reaction for optosensing mercury ions and bisphenol A'

# Authors
# If you created a profile for a user (e.g. the default `admin` user), write the username (folder name) here
# and it will be replaced with their full name and linked to their profile.
authors:
  - admin
  - Bui The Huy 
  - The Thuy T. Nguyen 
  - Nguyen Minh Vuong 
  - Eric Conte 
  - Yong-Ill Lee 

date: '2017-09-01T00:00:00Z'
doi: ''

# Schedule page publish date (NOT publication's date).
publishDate: '2022-04-18T00:00:00Z'

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ['2']

# Publication name and optional abbreviated publication name.
publication: Microchemical Journal 
publication_short: In *MJ*

abstract: A new approach with simple, fast and most versatile at low synthetic temperature is proposed for producing carbon nitride nanoparticles using ammonium citrate as both the nitrogen source and the framework for the formation of carbon nitride via solid state reaction. The prepared products are water soluble with high quantum yields of 27%. The initial assessments for optosensing of mercury ions and bisphenol A were performed based on the change in fluorescent intensity of the developed product. The mercury ions could be detected with a limit of detection of 60 nM with high selectivity based on the quenching effect. The detection on Bisphenol-A was carried out using “off-on” technique of fluorescent intensity with a limit of detection of 45 nM. 

# Summary. An optional shortened abstract.
summary: Tiny dots glow under U.V. light; glow less when near certain pollutants.  

tags: []

# Display this page in the Featured widget?
featured: true

# Custom links (uncomment lines below)
# links:
# - name: Custom Link
#   url: http://example.org

url_pdf: 'https://www-sciencedirect-com.ezproxy.lib.purdue.edu/science/article/abs/pii/S0026265X17303892'
url_code: ''
url_dataset: ''
url_poster: ''
url_project: ''
url_slides: ''
url_source: ''
url_video: ''

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
image:
  caption: 'Image credit: [**MJ**](https://www.sciencedirect.com/science/article/abs/pii/S0026265X17303892)'
  focal_point: ''
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
slides: ""
---

