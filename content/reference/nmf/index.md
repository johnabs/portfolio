---
title: 'Iterative Non-Negative Matrix Factorization Filter for Blind Deconvolution in Photon/Ion Counting'



# Authors
# If you created a profile for a user (e.g. the default `admin` user), write the username (folder name) here
# and it will be replaced with their full name and linked to their profile.
authors:
  - Scott R. Griffin 
  - admin
  - Casey J. Smith 
  - Ximeng Yuo-Dow 
  - Julia K. White 
  - Si-Wei Zhang 
  - Julie Novak 
  - Zhen Liu 
  - Garth J. Simpson 

date: '2019-03-11T00:00:00Z'
doi: ''

author_notes:
 - 'Equal Contribution'
 - 'Equal Contribution'

# Schedule page publish date (NOT publication's date).
publishDate: '2022-04-18T00:00:00Z'

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ['2']

# Publication name and optional abbreviated publication name.
publication: Analytical Chemistry 
publication_short: In *Anal Chem*

abstract: A digital filter based on non-negative matrix factorization (NMF) enables blind deconvolution of temporal information from large data sets, simultaneously recovering both photon arrival times and the instrument impulse response function (IRF). In general, the measured digital signals produced by modern analytical instrumentation are convolved by the corresponding IRFs, which can complicate quantitative analyses. Common examples include photon counting (PC), chromatography, super resolution imaging, fluorescence imaging, and mass spectrometry. Scintillation counting, in particular, provides a signal-to-noise advantage in measurements of low intensity signals, but has a limited dynamic range due to pulse overlap. This limitation can complicate the interpretation of data by masking temporal and amplitude information on the underlying detected signal. Typical methods for deconvolution of the photon events require advanced knowledge of the IRF, which is not generally trivial to obtain. In this work, a sliding window approach was developed to perform NMF one pixel at a time on short segments of large (e.g., 25 million point) data sets. Using random initial guesses for the IRF, the NMF filter simultaneously recovered both the deconvolved photon arrival times and the IRF. Applying the NMF filter to the analysis of triboluminescence (TL) data traces of active pharmaceutical ingredients enabled discrimination between different hypothesized physical origins of the signal. 

# Summary. An optional shortened abstract.
summary: We make a filter to find individual photons when crystals get smacked too hard. 

tags: []

# Display this page in the Featured widget?
featured: true

# Custom links (uncomment lines below)
# links:
# - name: Custom Link
#   url: http://example.org

url_pdf: 'https://pubs.acs.org/doi/pdf/10.1021/acs.analchem.9b00120'

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
image:
  caption: 'Image credit: [**Anal Chem**](https://pubs.acs.org/doi/pdf/10.1021/acs.analchem.9b00120)'
  focal_point: ''
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
slides: ""
---

