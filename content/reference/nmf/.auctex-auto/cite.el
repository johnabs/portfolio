(TeX-add-style-hook
 "cite"
 (lambda ()
   (LaTeX-add-bibitems
    "doi:10.1021/acs.analchem.9b00120"))
 :bibtex)

