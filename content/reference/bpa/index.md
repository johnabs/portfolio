---
title: 'Trap and release of bisphenol-A, 2-naphthol, and doxepin using a 1-hexadecylamine-copper(II)-amine functionalized indium-tin-oxide electrode'

# Authors
# If you created a profile for a user (e.g. the default `admin` user), write the username (folder name) here
# and it will be replaced with their full name and linked to their profile.
authors:
  - Guo-Rong Lin
  - admin
  - Elijah Ernst
  - Stuart Burris
  - Eric D. Conte
  - Wei-Ping Dow
  - Rong-Ho Lee
  - Shing-Yi Suen

date: '2016-07-01T00:00:00Z'
doi: ''

# Schedule page publish date (NOT publication's date).
publishDate: '2021-04-18T00:00:00Z'

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ['2']

# Publication name and optional abbreviated publication name.
publication: Chemical Engineering Journal 
publication_short: In *CEJ*

abstract: An electrically controlled and renewable hydrophobic surface used to trap and release three test pollutants on an indium-tin-oxide (ITO) electrode is presented. Oxygen plasma was used to maximize the activated hydroxyl groups on the ITO surface, which were reacted with (3-aminopropyl) triethoxysilane (APTES) to create a 3-amino propyl layer. Copper(II) ions were introduced and attached to the terminal amine groups. The amine group from 1-hexadecylamine (HDA), complexed the surface-confined copper(II) ions, which resulted in terminal hexadecyl alkyl groups. The created hydrophobic surface was tested for its ability to adsorb pollutant substances, namely bisphenol-A, 2-naphthol, and doxepin. Desorption was accomplished by reducing copper(II) to copper(0) electrochemically. This led to the weakening of the metal-amine complex and the concomitant detachment of the HDA and trapped species from the ITO surface. Efficient trapping and releasing of bisphenol-A (1 ppm) was achieved with around 84% adsorption, 100% desorption (in a time course of less than 30 s), and 42-fold enrichment on the concentration. After each adsorption/desorption trial, regeneration of the hydrophobic surface was achieved by reapplication of copper(II) ions and HDA. Five cycles were successfully repeated without any significant deterioration in performance. Similar trap-and-release performance was also attained for 2-naphthol and doxepin.

# Summary. An optional shortened abstract.
summary: Use modified, conductive glass to extract hydrophobic pollutants from water.

tags: []

# Display this page in the Featured widget?
featured: true

# Custom links (uncomment lines below)
# links:
# - name: Custom Link
#   url: http://example.org

url_pdf: 'https://www-sciencedirect-com.ezproxy.lib.purdue.edu/science/article/pii/S1385894716303059'
url_code: ''
url_dataset: ''
url_poster: ''
url_project: ''
url_slides: ''
url_source: ''
url_video: ''

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
image:
  caption: 'Image credit: [**CEJ**](https://www-sciencedirect-com.ezproxy.lib.purdue.edu/science/article/pii/S1385894716303059)'
  focal_point: ''
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
slides: ""
---

