---
# An instance of the Experience widget.
# Documentation: https://wowchemy.com/docs/page-builder/
widget: experience

# This file represents a page section.
headless: true

# Order that this section appears on the page.
weight: 40

title: Experience
subtitle: 'Research and Industrial'

# Date format for experience
#   Refer to https://wowchemy.com/docs/customization/#date-format
date_format: Jan 2006

# Experiences.
#   Add/remove as many `experience` items below as you like.
#   Required fields are `title`, `company`, and `date_start`.
#   Leave `date_end` empty if it's your current employer.
#   Begin multi-line descriptions with YAML's `|2-` multi-line prefix.
experience:
  - title: CTO
    company: Evergreen International 
    company_url: ''
    company_logo: org-gc
    location: Michigan 
    date_start: '2021-01-01'
    date_end: ''

  - title: Graduate Research Assistant in Industrial Engineering
    company: Purdue University 
    company_url: ''
    company_logo: purdue
    location: West Lafayette, Indiana
    date_start: '2020-08-01'
    date_end: ''
    description: Working in complex systems, network science, and more; still teaching a lot.

  - title: Implementation Engineer
    company: Bloomerang 
    company_url: ''
    company_logo: bloom 
    location: Indianapolis, Indiana 
    date_start: '2019-04-01'
    date_end: '2020-08-01'

  - title: Graduate Research Assistant in Chemistry 
    company: Purdue University 
    company_url: ''
    company_logo: purdue
    location: West Lafayette, Indiana
    date_start: '2015-08-01'
    date_end: '2019-05-01'
    description: Worked in analytical chemistry and nanotech for 4 years; taught a lot. 

  - title: NSF International Research Experience for Undergraduates 
    company: Changwon National University 
    location: Changwon, South Korea
    company_logo: cnu
    date_start: '2015-05-01'
    date_end: '2015-08-01'
    description: Developed carbon nitride quantum dot chemical sensors. 

  - title: Undergraduate Researcher 
    company: Western Kentucky University 
    company_logo: wku 
    location: Bowling Green, Kentucky 
    date_start: '2012-08-01'
    date_end: '2015-05-01'
    description: Worked on developing gold-based electrochemically resersible sorbents for pollution extraction 

  - title: NSF International Research Experience for Undergraduates 
    company: National Chung Hsing University 
    location: Taichung, Taiwan 
    company_logo: nchu 
    date_start: '2012-05-01'
    date_end: '2012-08-01'
    description: Worked with ITO based reversible sorbents for pollution extraction. 

design:
  columns: '2'
---
