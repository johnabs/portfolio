---
# An instance of the Accomplishments widget.
# Documentation: https://wowchemy.com/docs/page-builder/
widget: accomplishments

# This file represents a page section.
headless: true

# Order that this section appears on the page.
weight: 50

# Note: `&shy;` is used to add a 'soft' hyphen in a long heading.
title: 'Awards'
subtitle: 'and Accomplishments'

# Date format
#   Refer to https://wowchemy.com/docs/customization/#date-format
date_format: Jan 2006

# Accomplishments.
#   Add/remove as many `item` blocks below as you like.
#   `title`, `organization`, and `date_start` are the required parameters.
#   Leave other parameters empty if not required.
#   Begin multi-line descriptions with YAML's `|2-` multi-line prefix.
item:
  - date_start: '2022-01-01'
    organization: Purdue University 
    organization_url: https://www.coursera.org
    title: Estus H. And Vashti L. Magoon Award for Excellence in Teaching 
    url: 'https://engineering.purdue.edu/Engr/People/Awards/Graduate/ptRecipientListingCEA?group_id=266693&show_sub_groups=0'

  - date_end: '2019-08-01'
    date_start: '2015-08-01'
    organization: Purdue University Chemistry Department 
    title: Ross Recruitment Fellowship for Outstanding PhD Students
    url: https://www.purdue.edu/gradschool/fellowship/fellowship-resources-for-staff/managed-fellowships/recruitment-fellowships.html 

  - date_start: '2015-05-01'
    organization: Western Kentucky University 
    title: 'Outstanding Chemistry Major Award'
    url: ''

  - date_end: '2015-08-01'
    date_start: '2015-05-01'
    organization: National Science Foundation 
    title: 'NSF International Research Grant'
    url: ''

  - date_end: '2015-05-01'
    date_start: '2013-08-01'
    organization: The Gatton Academy 
    organization_url: https://www.wku.edu/academy/ 
    title: 'Gatton Academy of Mathematics and Science Distinguished Alumni Scholarship'
    description: Full-ride scholarship for my undergraduate degree due to my performance in the academy. 
    url: ''

  - date_start: '2013-08-01'
    organization: Elks National Foundation 
    title: Elk's National Scholar
    url: ''

  - date_end: '2013-08-01'
    date_start: '2013-05-01'
    organization: American Chemical Society 
    title: 'ACS Project SEED Grant'
    url: ''

  - date_end: '2012-08-01'
    date_start: '2012-05-01'
    organization: National Science Foundation 
    title: 'NSF International Research Grant'
    url: ''


design:
  columns: '2'
---
