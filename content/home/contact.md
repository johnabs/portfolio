---
# An instance of the Contact widget.
widget: contact

# This file represents a page section.
headless: true

# Order that this section appears on the page.
weight: 130

title: Contact
subtitle:

content:
  # Automatically link email and phone or display as text?
  autolink: true

  # Contact details (edit or remove options as required)
  email: jbiechel@purdue.edu
  #phone: 888 888 88 88
  address:
    street: 315 N Grant Street
    city: West Lafayette 
    region: IN
    postcode: '47906'
    country: United States
    country_code: US
  #coordinates:
  #  latitude: '37.4275'
  #  longitude: '-122.1697'
  #directions: Enter Grissom Hall and take the elevator floor 2 
  #office_hours:
  #  - 'Monday 10:00 to 13:00'
  #  - 'Wednesday 09:00 to 10:00'
  appointment_url: 'https://calendly.com/johnabs'

design:
  columns: '2'
---
