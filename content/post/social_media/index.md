---
title: Reasons I'm Leaving (traditional) Social Media
displayInMenu: false 
displayInList: true 
dropCap: false 
date: '2021-01-07'
slug: Reasons I'm Leaving Social Media
tags:
  - Philosophy 
  - Software 
  - Politics 
---


Polarization: division into two sharply distinct opposites. 

Privacy: the state of being free from public attention or unsanctioned intrusion.

Attention: the ability or power to keep the mind on something.

These are the primary reasons I'm leaving the more traditional social media sites. In this article, I'll be making the case for why these traditional sites move us in a direction away from the optimal positions in these three important areas of modern living, and I'll be linking alternative tools below that supplement or replace these original websites more effectively.

## The Problem

Facebook, Twitter, Youtube, and Google, and most social media, are all guilty of reducing the odds that people will encounter facts or opinions that may disagree with their own worldview. The strategy behind this, is that content, unchallenged individuals are comfortable individuals, who are much more likely to be swayed by the advertisements these companies serve. Even if you were not more likely to actually engage with any particular advertisement (which is likely the case), it does increase your retention time, thus increase the number of dice rolls these companies have until they hit the jackpot and serve you an advertisement you're actually interested in investigating. Then, when you click it, another piece of their model of you gets filled in, which makes them more likely to serve you more relevant ads in the future, thus insuring future profits. This by itself wouldn't necessarily be a bad thing; however, when considered in light of the fact that they intentionally try to increase your retention (i.e the time you spend browsing) via the same cognitive tricks that incite gambling addictions, it becomes significantly more insidious. 

These companies also collect untold amounts of data through your browsing habits, and use that to build a profile of your likes and dislikes. They do this not only on their own websites, but on the websites of other companies or individuals, who may or may not even disclose to you that they are using these services to continue their surveillance. This essentially means that you must be particularly careful in what services you select if you don't want to be tracked, which is in nearly all cases impossible. My website doesn't use any of these metrics; however, that means that without direct donations, I am incapable of generating any revenue from these posts. I am okay with this though, as I don't want to violate the privacy of the people who want to read what I have to say.

As implied above, these companies are in the human attention market. They want to maximize the amount of your attention they receive to maximize their ad revenue, and do this by keeping you in a happy little bubble where you don't encounter opposing viewpoints, unless they are couched in a demonizing way, which further cements your existing views. This leads to the polarization I mentioned above, to the point that Facebook has at least been correlated to increased polarization to the point of genocides and revolution in many countries. Even worse, a majority of users who join extermist groups on Facebook do so because the algorithm recommended it to them, yet Facebook is allowed to continue these unacceptable practices.

A society in which people are incapable of contending with ideas they don't like is not a stable one. It leads to moral outrage being equated with virtue, and intolerance of differing views being confused with moral superiority. Additionally, many articles posted on these sites (which are designed to attract clicks rather than disseminate information) intentionally spread falsehoods and deceive the population. This then leads to both a post-truth and post-reason society. Truth is the fundamental foundation upon which all decisions must begin, and reason the mechanism by which these decisions are achieved. Without either, we become untethered from the shore, and are tossed about by the waves of emotionality and irrationality. This maelstrom of reactionary rage will ultimately replace society's cohesive structure of shared customs, ethics, etc., for a structure of insular groups that primarily interact out of antagonistic motivation. Unfortunately, we're already beginning to see some of this with the increasing drift in reporting the news, the irrational behavior from the people who stormed the capitol, and the further stoking of the flames by the legislators who were targeted by the mob. It's almost as if the rhetoric is taking priority over peace, which is a dangerous precident to set.

While I do not believe that these platforms are the sole reason we are moving towards a direction of instability, it seems apparent that they are certainly a contributing factor, even if only minorly. Thus I have resolved to remove them or replace them with alternatives that respect my privacy, attention, and my desire to be exposed to the views of others, so that I may more readily engage with these people, even if I disagree with them.

## Alternatives

For Youtube, I use an instance known as [Invidious](https://invidious.xyz) (or NewPipe on Android), which is a front-end for Youtube that doesn't curate content, but allows you to search for anything you want. This lack of curation means you're less likely to be "sucked down a rabbit-hole" and lose a significant portion of your attention for the day. Additionally, you have access to all the same content that you normally would on Youtube, but without the tracking. Other alternatives with alternative content include [BitChute](https://bitchute.com), [LBRY](https://lbry.tv), and more.

For Twitter, I've switched over to [Mastodon](https://mastodon.online), a federated (not centralized) social media platform that replaces "Tweets" with "Toots" (since a mastodon is an ancient elephant).  It is fully open-source, doesn't track you, and enables you to follow or block whomever you want, again without the content curation. Additionally, certain sub-servers such as [qoto](https://qoto.org), allow you to post significantly longer form "Toots", which I find allows users to write more complex ideas and reduce ambiguity in the messages we try to convey.

For Facebook, I've switched over to [Element](https://element.io). While not a direct replacement (it's more like a replacement for Slack/Disord), it replaces what I use Facebook for nowadays, which is primarily the messenger feature, as well as video/audio calls.

For Google, I'd suggest [DuckDuckGo](https://duckduckgo.com), [Snopyta](https://search.privacytools.io/searx/), [Qwant](https://www.qwant.com), [Ecosia](https://www.ecosia.org), etc. There are so many different search alternatives that you can use, which are all excellent. Of the services Google provides that I still use regularly, Maps is the only one I can think of, and I'm testing [Open Street Map](https://www.openstreetmap.org) (which has a GPS enabled app on Android called OSMAnd+) as an alternative.


## Conclusion

While I understand that many people believe that these platforms are just "part of our lives now", I hope to have illustrated reasons this should not be the case, and the fact that better alternatives exist. Better still, these alternatives were not designed to be addictive and still allow us to interface and be social with one another; thus, I think it's unreasonable to continue using these original services. I hope that even if you continue to use them, you consider the alternatives, and perhaps try migrating a little bit to see what you think. I think you'll find, as I have, that these "services" aren't serving you as much as you are them, and that you have the power to take back control of your online life while maintaining contact with your friends and family in a convenient way.
